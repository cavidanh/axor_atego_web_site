from django.urls import path
from about_app.views import *

app_name = 'about_app'

urlpatterns = [
    path('',about_view, name='about_url')
]