from django.db import models


# Create your models here.


class AboutusModel(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    image = models.ImageField(blank=True)
    text = models.TextField(null=True, blank=True)

    def __str__(self):
        return f'{self.title}'

class ChoiceModel(models.Model):
    about_id = models.ForeignKey('AboutusModel', default=None, on_delete=models.CASCADE)
    work_choices = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.about_id}'
