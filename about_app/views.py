from django.shortcuts import render
from about_app.models import *


# Create your views here.

def about_view(request):
    context = {
        'all_data': AboutusModel.objects.all(),
        'choice_datas': ChoiceModel.objects.all()
    }
    return render(request, 'about.html', context)
