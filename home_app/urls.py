from django.urls import path
from home_app.views import *

app_name = 'home_app'

urlpatterns = [
    path('',home_view, name='home_url')
]