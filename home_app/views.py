from django.shortcuts import render
from home_app.models import *
from gallery_app.models import *


# Create your views here.

def home_view(request):
    context = {
        'slider_info': HomeSlider.objects.all()[:3],
        'phone_info': ContactHome.objects.all()[:1],
        'our_services': OurServices.objects.all()[:2],
        'experience_info': ExperienceInfo.objects.all()[:3],
        'footer_data': Footer.objects.all(),
        'gallery': Gallery.objects.all()[:3]

    }

    return render(request, 'home_page.html', context)
