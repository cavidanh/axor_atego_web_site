from django.db import models


# Create your models here.

class HomeSlider(models.Model):
    slide_image = models.ImageField(upload_to='media/slider_home/')
    header_name = models.CharField(max_length=100)
    header_name_bold = models.CharField(max_length=100)
    slide_text = models.TextField(max_length=200)

    def __str__(self):
        return f'{self.header_name}'


class ContactHome(models.Model):
    time_text = models.CharField(max_length=100)
    number = models.CharField(max_length=14)
    info_text = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.number}'


class OurServices(models.Model):
    icon = models.ImageField(upload_to='media/our_services_icons/')
    name = models.CharField(max_length=100)
    text = models.TextField()

    def __str__(self):
        return f'{self.name}'


class ExperienceInfo(models.Model):
    work_experience = models.CharField(max_length=6)
    customer_count = models.CharField(max_length=6)
    worker_team_count = models.CharField(max_length=6)

    def __str__(self):
        return f'work experience {self.work_experience} | customer count {self.customer_count} | work team count {self.worker_team_count}'


class Footer(models.Model):
    text = models.TextField()
    phone_number = models.CharField(max_length=14)
    whatsapp_number = models.CharField(max_length=14)
    time_text = models.CharField(max_length=100)
    my_location = models.CharField(max_length=255)
    email = models.EmailField()
    instagram_link = models.URLField()
    facebook_link = models.URLField()
    google_map_link = models.URLField(null=True, blank=True)

    def __str__(self):
        return f'{self.time_text}'
