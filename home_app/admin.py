from django.contrib import admin
from home_app.models import *
# Register your models here.
admin.site.register(HomeSlider)
admin.site.register(ExperienceInfo)
admin.site.register(OurServices)
admin.site.register(Footer)
admin.site.register(ContactHome)

