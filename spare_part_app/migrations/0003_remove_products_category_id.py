# Generated by Django 3.0.8 on 2020-08-11 07:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spare_part_app', '0002_auto_20200811_0733'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='products',
            name='category_id',
        ),
    ]
