# Generated by Django 3.0.8 on 2020-08-11 08:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spare_part_app', '0010_auto_20200811_0811'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='products',
            name='subcategories_id',
        ),
        migrations.DeleteModel(
            name='SubCategories',
        ),
    ]
