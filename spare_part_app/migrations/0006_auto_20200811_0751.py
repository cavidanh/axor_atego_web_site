# Generated by Django 3.0.8 on 2020-08-11 07:51

from django.db import migrations
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('spare_part_app', '0005_auto_20200811_0750'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subcategories',
            name='parent_id',
            field=mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='spare_part_app.Categories'),
        ),
    ]
