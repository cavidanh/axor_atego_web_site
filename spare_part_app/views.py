from django.shortcuts import render
from spare_part_app.models import Categories, Products, ProductImages
from home_app.models import Footer


# Create your views here.
def spare_part_view(request):
    context = {
        'categories': Categories.objects.all(),
        'product_list': Products.objects.all(),
        'footer_data': Footer.objects.all()
    }
    return render(request, 'spare_part.html', context)


def spare_part_filter_view(request, name):
    context = {
        'categories': Categories.objects.all(),
        'product_list': Products.objects.filter(category_id__category_name=name),
        'footer_data': Footer.objects.all()
    }

    return render(request, 'spare_part.html', context)


def spare_part_detail_view(request,category_name,product_id):
    context = {
        'product_detail': Products.objects.filter(category_id__category_name=category_name, id=product_id),
        'product_images': ProductImages.objects.filter(product_id__category_id__category_name=category_name, product_id=product_id),
        'footer_data': Footer.objects.all()
    }

    return render(request, 'spare_part_detail.html',context)
