from django.db import models
# from mptt.models import MPTTModel, TreeForeignKey

# Create your models here.


class Categories(models.Model):
    category_name = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.category_name}'


class Products(models.Model):
    category_id = models.ForeignKey('Categories', on_delete=models.CASCADE)
    product_name = models.CharField(max_length=100)
    product_description = models.CharField(max_length=255)
    image = models.ImageField(blank=True)
    price = models.CharField(max_length=20, null=True, blank=True)
    about_product = models.TextField()

    def __str__(self):
        return f'Category | {self.category_id}'


class ProductImages(models.Model):
    product_id = models.ForeignKey('Products',default=None, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='media/ProductIMages')

    def __str__(self):
        return f'{self.product_id}'