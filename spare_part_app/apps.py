from django.apps import AppConfig


class SparePartAppConfig(AppConfig):
    name = 'spare_part_app'
