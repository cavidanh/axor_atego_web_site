from django.contrib import admin
from spare_part_app.models import *

# Register your models here.

admin.site.register(Categories)


class ProductImageAdmin(admin.StackedInline):
    model = ProductImages


@admin.register(Products)
class ProductAdmin(admin.ModelAdmin):
    inlines = [ProductImageAdmin]

    # class Meta:
    #     model = Products


@admin.register(ProductImages)
class ProductImageAdmin(admin.ModelAdmin):
    pass
