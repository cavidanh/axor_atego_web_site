from django.urls import path
from spare_part_app.views import *

app_name = 'spare_part_app'

urlpatterns = [
    path('',spare_part_view, name='spare_part_url'),
    path('<slug:name>',spare_part_filter_view, name='spare_part_filter_view'),
    path('spare_part_detail/<slug:category_name>/<slug:product_id>/',spare_part_detail_view, name='spare_part_detail_url'),
]