from django.urls import path
from contact_app.views import *

app_name = 'contact_app'

urlpatterns = [
    path('',contact_view, name='contact_url')
]