from django.shortcuts import render
from home_app.models import *


# Create your views here.

def contact_view(request):
    context = {
        'footer_data': Footer.objects.all()
    }
    return render(request, 'contact.html', context)
