from django.urls import path
from gallery_app.views import *

app_name = 'gallery_app'

urlpatterns = [
    path('',gallery_view, name='gallery_url'),
    path('<slug:name>',gallery_filter, name='gallery_filter')
]