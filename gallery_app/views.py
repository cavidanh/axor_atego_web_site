from django.shortcuts import render, redirect
from gallery_app.models import *
from home_app.models import *


# Create your views here.

def gallery_view(request):
    context = {
        'categories': Categories.objects.all(),
        'filter_image': Gallery.objects.all(),
        'footer_data': Footer.objects.all()
    }
    return render(request, 'gallery.html', context)


def gallery_filter(request, name):
    context = {
        'categories': Categories.objects.all(),
        'filter_image': Gallery.objects.filter(category_id__category_name=name),
        'footer_data': Footer.objects.all()
    }

    return render(request, 'gallery.html', context)
