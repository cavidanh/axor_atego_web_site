from django.db import models

# Create your models here.


class Categories(models.Model):
    category_name = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.category_name}'


class Gallery(models.Model):
    category_id = models.ForeignKey('Categories',on_delete=models.CASCADE)
    image = models.ImageField(upload_to='media/Gallery')

    def __str__(self):
        return f'Category | {self.category_id}'
