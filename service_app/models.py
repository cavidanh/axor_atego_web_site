from django.db import models


class ServicesModel(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(blank=True)
    text_1 = models.TextField(null=True,blank=True)
    text_2 = models.TextField(null=True,blank=True)
    text_3 = models.TextField(null=True,blank=True)
    text_4 = models.TextField(null=True,blank=True)

    def __str__(self):
        return f'Category | {self.title}'


class ServicesImages(models.Model):
    about_id = models.ForeignKey('ServicesModel', default=None, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='media/ProductIMages')

    def __str__(self):
        return f'{self.about_id}'


class ServiceModel(models.Model):
    about_id = models.ForeignKey('ServicesModel', default=None, on_delete=models.CASCADE)
    services = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.about_id}'
