from django.contrib import admin
from service_app.models import *


# Register your models here.

class ServiceImageAdmin(admin.StackedInline):
    model = ServicesImages


class ServicesAdmin(admin.TabularInline):
    model = ServiceModel


@admin.register(ServicesModel)
class ProductAdmin(admin.ModelAdmin):
    inlines = [ServiceImageAdmin,ServicesAdmin]

    # class Meta:
    #     model = Products


#
@admin.register(ServicesImages)
class ServiceImageAdmin(admin.ModelAdmin):
    pass

#################################################33
