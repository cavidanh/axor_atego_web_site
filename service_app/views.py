from django.shortcuts import render
from service_app.models import *
from home_app.models import *


# Create your views here.

def service_view(request):
    context = {
        'services_data': ServicesModel.objects.all(),
        'services_image': ServicesImages.objects.all(),
        'services': ServiceModel.objects.all(),
        'footer_data': Footer.objects.all()
    }
    return render(request, 'service.html', context)
