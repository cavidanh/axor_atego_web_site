from django.urls import path
from service_app.views import *

app_name = 'service_app'

urlpatterns = [
    path('',service_view, name='service_url')
]